<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAliceLogTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('alice_logs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->dateTime('moment');
            $table->string('result', 32);

            $table->string('command', 1024);
            $table->string('reply', 1024)->nullable();

            $table->json('session')->nullable();
            $table->json('request')->nullable();
            $table->text('extra')->nullable();
            $table->string('device_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('alice_logs');
    }
}
