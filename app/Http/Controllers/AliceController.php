<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Dialogs\AliceRequest;
use App\Http\Controllers\Dialogs\AliceResponse;
use App\Http\Controllers\Dialogs\SkolkovoMMA;
use App\Models\AliceLog;
use Illuminate\Http\Request;

class AliceController extends Controller {

//    public function test($code){
//
//        $hotel = Hotel::siteCode($code)->first();
//        $aliceSession = new FishpointAliseDialog($hotel);
//
//        $e = $aliceSession->findProduct('такси');
//
//        print_r($e); die;
//
//    }


    public function index(Request $request) {

        $aRequest = new AliceRequest($request);
        $aResponce = new AliceResponse($aRequest);

        if (!$aResponce->isEmpty()) {
            return $aResponce->build();
        }

        $dialog = $this->buildDialog($aRequest, $aResponce);

        try {

            $dialog->preProcess();
            $result = $dialog->process($aRequest, $aResponce);
            if (is_string($result)) {
                $aResponce->tts($result);
            }
            $dialog->postProcess();
            $dialog->log->reply = mb_substr($aResponce->_tts, 0, 1024);
            $dialog->log->save();
        } catch (\Exception $e) {
            $dialog->log->result = AliceLog::RESULT_ERROR;
            $dialog->log->extra = $e->getMessage() . "\n\n" . $e->getTraceAsString();
            $dialog->log->save();
        }

        return $aResponce->build();

    }

    private function buildDialog(AliceRequest $request, AliceResponse $response) {

        $dialog = new SkolkovoMMA();

        $log = AliceLog::create([
            'moment' => now(),
            'command' => substr($request->command, 0, 1024),
            'reply' => null,
            'request' => $request->request,
            'session' => null,
            'result' => AliceLog::RESULT_OK,
            'extra' => null,
            'device_id' => $request->deviceId
        ]);

        $dialog->in = $request;
        $dialog->out = $response;
        $dialog->log = $log;

        return $dialog;
    }



}
