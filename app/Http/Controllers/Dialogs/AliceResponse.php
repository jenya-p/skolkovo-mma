<?php

namespace App\Http\Controllers\Dialogs;


class AliceResponse {

    var $_text;
    var $_tts;
    var $_theme;
    var $_endSession = false;

    var $session = [];
    var $application = [];

    public function __construct(AliceRequest $r) {
        $this->session = $r->session;
        $this->application = $r->application;
    }

    public function text($text) {
        $this->_text = $text;
        return $this;
    }

    public function tts($ttl) {
        $this->_tts = $ttl;
        return $this;
    }

    public function end($end = true) {
        $this->_endSession = $end;
        return $this;
    }

    public function theme($theme) {
        $this->_theme = $theme;
        return $this;
    }

    public function isEmpty(){
        return empty($this->_tts);
    }

    public function build() {

        if ($this->isEmpty()) {

            $this->_text = 'Что то пошло не так';
            $this->_tts = 'Что то пошло не так';

        } else if (empty($this->_text)) {
            $this->_text = preg_replace('/sil <\[\d{2,4}\]> /', ' ', $this->_tts);
            $this->_text = str_replace('+', '', $this->_text);
        }


        $this->session['theme'] = $this->_theme;

        $response = [
            "response" => [
                "text" => $this->_text,
                "tts" => $this->_tts,
                "end_session" => $this->_endSession,
            ],
            "version" => "1.0"
        ];

        $response['application_state'] = empty($this->application) ? new \stdClass() : $this->application;
        $response['session_state'] = empty($this->session) ? new \stdClass() : $this->session;

        return $response;
    }

}
