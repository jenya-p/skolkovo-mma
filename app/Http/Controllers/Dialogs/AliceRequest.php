<?php

namespace App\Http\Controllers\Dialogs;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AliceRequest {
    /** @var array|null */
    var $request = null;

    var $deviceId = null;
    var $command = '';
    var $isNew = false;
    var $session = [];
    var $application = [];
    var $applicationId = null;
    var $theme = null;


    public function __construct($request) {

        if(is_array($request)){
            $this->request = $request;
        } else if(is_object($request) && $request instanceof Request){
            $this->request = json_decode($request->getContent(), true);
        } else {
            throw new \Exception('incorrect request 1');
        }

        if (empty($this->request['session']) || empty($this->request['session']['application']) || empty($this->request['meta'])
            || empty($this->request['request']) || $this->request['request']['type'] !== 'SimpleUtterance') {
            throw new \Exception('incorrect request 2');
        }
        $this->deviceId = @$this->request['meta']['device_id'];
        $this->applicationId = @$this->request['session']['application']['application_id'];
        $this->command = @$this->request['request']['command'];
        $this->isNew = @$this->request['session']['new'];

        if(isset($this->request['state'])){
            if(isset($this->request['state']['session'])){
                $this->session = $this->request['state']['session'];
                $this->theme = @$this->request['state']['session']['theme'];
            }
            if(isset($this->request['state']['application'])){
                $this->application = $this->request['state']['application'];
            }
        }


    }

    public function getValuesByEntityType($type){
        if ($this->request['request']['nlu']['entities']) {
            foreach ($this->request['request']['nlu']['entities'] as $entity) {
                if ($entity['type'] == $type) {
                    yield $entity['value'];
                }
            }
        }
    }

    public function getValueByEntityType($type, $ind = 0){
        if(!empty($this->request['request']['nlu']['entities'])){
            foreach ($this->request['request']['nlu']['entities'] as $entityItem){
                if($entityItem['type'] == $type){
                    if($ind === 0){
                        return $entityItem['value'];
                    } else {
                        $ind--;
                    }
                }
            }
        }
        return null;
    }

    public function hasTheme($theme){
        return $this->theme == $theme;
    }




    public function hasIntent($intent = null){
        if($intent === null) {
            return !empty($this->request['request']['nlu']['intents']);
        } else {
            return !empty($this->request['request']['nlu']['intents']) && array_key_exists($intent, $this->request['request']['nlu']['intents']);
        }
    }

    public function intentSlot($intent, $slot){
        return @$this->request['request']['nlu']['intents'][$intent]['slots'][$slot]['value'];
    }

    public function getMatchedIntent($expr){
        if(empty($this->request['request']['nlu']['intents'])){
            return false;
        };
        foreach ($this->request['request']['nlu']['intents'] as $key => $intent){
            $matches = [];
            if(preg_match('/' . $expr . '/', $key, $matches)){
                return $matches;
            }
        }
        return false;
    }


    public function has() {
        foreach (func_get_args() as $word) {
            $word = str_replace(' * ', ' (\S+ )*', $word);
            if (preg_match('/(\s|^)(' . $word . ')(\s|$)/u', $this->command)) {
                return true;
            }
        }
        return false;
    }

    public function hasAll(){
        foreach (func_get_args() as $word) {
            $word = str_replace(' * ', ' (\S+ )*', $word);
            if (!preg_match('/(\s|^)(' . $word . ')(\s|$)/u', $this->command)) {
                return false;
            }
        }
        return true;
    }

    public function countExcept(){
        $count = substr_count($this->command, ' ') + 1;
        foreach (func_get_args() as $word) {
            $cMinus = substr_count($word, ' ') + 1;
            if (!preg_match('/(\s|^)(' . $word . ')(\s|$)/u', $this->command)) {
                $count -= $cMinus;
            }
        }
        return max(0, $count);
    }

    public function regExp(){
        foreach (func_get_args() as $word){
            if(preg_match($word, $this->command)){
                return true;
            }
        } return false;
    }

    public function setCommand($command){
        $this->command = mb_strtolower($command);
        $this->request['request']['command'] = mb_strtolower($command);
    }

}
