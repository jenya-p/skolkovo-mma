<?php
namespace App\Http\Controllers\Dialogs\Traits;

use App\Http\Controllers\Dialogs\AliceRequest;
use App\Http\Controllers\Dialogs\AliceResponse;
use App\Models\AliceLog;
use Illuminate\Support\Facades\Cache;

trait AbstractDialog {

    /** @var AliceRequest */
    var $in;
    /** @var AliceResponse */
    var $out;
    /** @var AliceLog */
    var $log;

    /** @alice-session */
    var $failCount = 0;
    /** @alice-session */
    var $theme = 'hello';
    /** @alice-application  */
    var $familiar = false;


    public function preProcess(){
        $sessionCacheKey = 'alice_dialogs_' . $this->in->applicationId;
        $localSession = Cache::get($sessionCacheKey, []);

        $r = new \ReflectionClass($this);
        /** @var \ReflectionProperty $property */
        foreach ($r->getProperties() as $property) {
            if(preg_match('/@alice-(application|session|local)[^a-z]/', $property->getDocComment(), $matches)){
                $name = $property->getName();
                if($matches[1] == 'application' && array_key_exists($name, $this->in->application)){
                    $this->$name = $this->in->application[$name];

                } else if($matches[1] == 'session' && array_key_exists($name, $this->in->session)){
                    $this->$name = $this->in->session[$name];

                } else if($matches[1] == 'local' && array_key_exists($name, $localSession)){
                    $this->$name = $localSession[$name];
                }
            }
        }
    }

    public function postProcess(){
        $r = new \ReflectionClass($this);

        $localSession = [];

        /** @var \ReflectionProperty $property */
        foreach ($r->getProperties() as $property) {
            if(preg_match('/@alice-(application|session|local)[^a-z]/', $property->getDocComment(), $matches)){
                $name = $property->getName();
                if($matches[1] == 'application'){
                    $this->out->application[$name] = $this->$name;

                } else if($matches[1] == 'session'){
                    $this->out->session[$name] = $this->$name;

                } else if($matches[1] == 'local') {
                    $localSession[$name] = $this->$name;
                }
            }
        }

        $sessionCacheKey = 'alice_dialogs_' . $this->in->applicationId;
        if(!empty($localSession)){
            Cache::put($sessionCacheKey, $localSession);
        } else {
            Cache::delete($sessionCacheKey);
        }

    }

    public function log($result){
        $this->log['result'] = $result;
    }


}
