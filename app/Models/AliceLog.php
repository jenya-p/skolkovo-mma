<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AliceLog
 * @package App
 *
 * @property int $id
 * @property int $moment
 * @property string $command
 * @property string $reply
 * @property int $hotel_id
 * @property string $room
 * @property string $result
 * @property array $session
 * @property array $request
 * @property array $extra
 *
 */
class AliceLog extends Model {

    public $timestamps = false;

    protected $dates = ['moment'];

    protected $casts = [
        'session' => 'array',
        'request' => 'array'
    ];

    const RESULT_OK = 'ok';
    const RESULT_UNRECOGNIZED = 'unrecognized';
    const RESULT_ERROR = 'error';
    const RESULT_WARN = 'warn';


    protected $fillable = ['moment','command','reply','result','session','request','extra','device_id'];


}
